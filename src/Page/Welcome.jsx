import React from "react";
import { Container, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Welcome({ checkSign }) {
  return (
    <Container>
      <br />
      <h1>Welcome</h1>
      <br />
      <Button onClick={checkSign}>Sign-out</Button>
    </Container>
  );
}

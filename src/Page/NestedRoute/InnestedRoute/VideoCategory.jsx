import React from 'react'
import { Container } from 'react-bootstrap'
import { useLocation } from 'react-router'

export default function VideoCategory() {

    const location = useLocation()
    const query = new URLSearchParams(location.search)
    const name = query.get("name")

    return (
        <Container>
            <br/>
            <h1>This is your choice: <span style={{color:"Red"}}>{name}</span></h1>
        </Container>
    )
}

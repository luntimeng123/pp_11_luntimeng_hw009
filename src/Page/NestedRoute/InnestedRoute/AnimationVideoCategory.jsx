import React from 'react'
import { Container } from 'react-bootstrap'
import { useLocation } from 'react-router'

export default function ActionA() {

    //in order to use RequestParam ,we need help from useLocation Because it have "search" property that can store value requestParam
    const location = useLocation()
    //we create to store "search" property of useLocation()
    const query = new URLSearchParams(location.search) //TODO:when serach get in key value format like : ?key=:value and now the key is type
    const name = query.get("type")


    return (
        <Container>
            <br/>
            <h1>This your choice: <span style={{color:"Red"}}>{name}</span></h1>
        </Container>
    )
}

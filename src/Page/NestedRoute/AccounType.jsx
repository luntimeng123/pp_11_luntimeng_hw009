import React from 'react'
import { Container } from 'react-bootstrap';
import { useParams } from 'react-router'

export default function AccounType() {

    const param = useParams();


    return (
        <Container>
            <br/>
            <h1>Account Type: <span style={{color:"Red"}}>{param.type} </span></h1>
        </Container>
    )
}

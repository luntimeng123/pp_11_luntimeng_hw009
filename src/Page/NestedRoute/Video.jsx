import React,{useState} from "react";
import { ButtonGroup, Container, Button } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import VideoCategory from "./InnestedRoute/VideoCategory";


export default function Video() {

    //const [tex,setTex] = useState("")


  return (
    <Router>
      <Container>
        <br />
        <h1>Video Category</h1>
        <br />
        <ButtonGroup aria-label="Basic example">
            {/* <input type="text" name="tex" onChange={(e)=>setTex(e.target.value)} placeholder="find.."></input>
            <br/> */}
            <Button as={Link} to="/VideoCategory?name=Adventure" variant="secondary">Adevnture</Button>
            <Button as={Link} to="/VideoCategory?name=Action" variant="secondary">Action</Button>
            <Button as={Link} to="/VideoCategory?name=Crime" variant="secondary">Crime</Button>
            <Button as={Link} to="/VideoCategory?name=Comedy" variant="secondary">Comedy</Button>
            <Button as={Link} to="/VideoCategory?name=Romance" variant="secondary">Romance</Button>
        </ButtonGroup>
      </Container>
      <Switch>
          <Route path="/VideoCategory" render={()=><VideoCategory/>}/>
      </Switch>
    </Router>
  );
}

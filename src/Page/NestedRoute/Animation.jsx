import React from "react";
import { ButtonGroup, Container, Button } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import AnimationVideoCategory from './InnestedRoute/AnimationVideoCategory';

export default function Animation() {

  return (
    <Router>
      <Container>
        <br />
        <h1>Animation Category</h1>
        <br />
        <ButtonGroup aria-label="Basic example">
        {/* //TODO:when we assign data like (?type=value), it is ?key=value format --> now key is "type" */}
          <Button as={Link} to="/animationVideoC?type=Action" variant="secondary">
            Action
          </Button>
          <Button as={Link} to="/animationVideoC?type=Comedy" variant="secondary">
            Comedy
          </Button>
          <Button as={Link} to="/animationVideoC?type=Romance" variant="secondary">
            Romance
          </Button>
        </ButtonGroup>
      </Container>
      <Switch>
        <Route path="/animationVideoC" render={()=><AnimationVideoCategory/>}/>
      </Switch>
    </Router>
  );
}

import React, { useEffect,useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import {Card, Container,Button} from 'react-bootstrap'

export default function Product({arrpic}) {

    const [newarr,setNewArr] = useState({})
    
    const param = useParams();
    useEffect(()=>{
        console.log(param);
        getDataById()
    },[])

    const getDataById = () => {
        const time = [...arrpic]
          
        for(let i=0;i<time.length;i++){
            if(time[i].id == param.id){
                setNewArr(time[i]) 
            }
        }
    }

    const history = useHistory();

    const back = () => {
        history.goBack();
    }

    return (
        <Container>
            <br/>
            <Button onClick={back}>Back</Button>
            <br/>
            <h1>This a product: {newarr.id}</h1>
            <br/>
            <Card style={{ width: "28rem",marginTop:"50px"}}>
                <Card.Img variant="top" src={newarr.url} />
                <Card.Body>
                  <Card.Title>{newarr.title}</Card.Title>
                </Card.Body>
              </Card>
        </Container>
    )
}

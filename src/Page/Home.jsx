import { Button, Card, Container } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";

export default function Home({ arrpic }) {
  //const [id, setId] = useState([1, 2, 3, 4]);

  const histtory = useHistory();
  const his = () => {
    histtory.push("/allproduct");
  };

  const back = () => {
    histtory.goBack();
  };

  useEffect(() => {
    console.log("UseEffect...");
  }, []);




  
  return (
    <Container>
      <h1>Home</h1>
      <div
        style={{ display: "flex", flexFlow: "row wrap", marginRight: "10px" }}
      >
        {arrpic.map((item, index) => (
          <a key={index}>
            <Link to={`/product/${item.id}`}>
              <Card style={{ width: "18rem", margin: "5px" }}>
                <Card.Img variant="top" src={item.url} alt="image" />
                <Card.Body>
                  <Card.Title>{item.title}</Card.Title>
                </Card.Body>
              </Card>
            </Link>
          </a>
        ))}
      </div>{" "}
      {/* how to use Button Click in Router */}
      <Button onClick={his}>Click</Button> <Button onClick={back}>Back</Button>
    </Container>
  );
}

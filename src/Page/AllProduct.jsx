import React from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AccounType from "./NestedRoute/AccounType";

export default function AllProduct() {
  return (
    <Router>
      <Container>
        <h1>Account</h1>
        <ul>
          <li>
            <Link to={`/allproduct/account/${"netflix"}`}>Netflix</Link>
          </li>
          <li>
            <Link to={`/allproduct/account/${"facebook"}`}>Facebook</Link>
          </li>
          <li>
            <Link to={`/allproduct/account/${"google"}`}>Google</Link>
          </li>
          <li>
            <Link to={`/allproduct/account/${"microsft"}`}>Microsoft</Link>
          </li>
        </ul>
        <br />
      </Container>
      <Switch>
        <Route path="/allproduct/account/:type" render={() => <AccounType />} />
      </Switch>
    </Router>
  );
}

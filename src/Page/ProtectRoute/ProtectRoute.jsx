import React,{Component} from 'react'
import {Redirect} from 'react-router'





export default function ProtectRoute({isSignIn,component:Component,path,checkSign}) {

    if(isSignIn){
        return (
            <Component path={path} checkSign={checkSign}/>
        )
    }else{
        return(
            <Redirect to="/auth"/>
        )
    }

}

import React from "react";
import { Container, Button, ButtonGroup } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Link } from "react-router-dom";
import Video from "./NestedRoute/Video";
import Animation from "./NestedRoute/Animation";







export default function Service() {
  return (
    <Router>
      <Container>
        <br />
        <h1>Video Type</h1>
        <br />
        <ButtonGroup>
            <Button as={Link} to="/service/video" >Video</Button>
            <Button as={Link} to="/service/animate">Animation</Button>
        </ButtonGroup>
      </Container>
      <Switch>
        <Route path="/service/video" render={() => <Video />} />
        <Route path="/service/animate" render={() => <Animation />} />
      </Switch>
    </Router>
  );
}

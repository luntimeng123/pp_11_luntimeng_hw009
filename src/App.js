import React, { useEffect, useState } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from "./Component/Menu";
import Home from "./Page/Home";
import Service from "./Page/Service";
import AllProduct from "./Page/AllProduct";
import Product from "./Page/Product";
import Auth from "./Page/Auth";
import AccounType from "./Page/NestedRoute/AccounType";
import ProtectRoute from "./Page/ProtectRoute/ProtectRoute";
import Welcome from "./Page/Welcome";

// import AxiosData from './Component/AxiosData'

export default function App() {
  const [arrpic, setArrPic] = useState([]);
  const [isSignIn,setIsSignIn] = useState(false)
  
  const checkSign = () => {
      setIsSignIn(!isSignIn);
  }

  useEffect(() => {
    getDataFromAPi();
    console.log(arrpic);
  }, []);

  const getDataFromAPi = async () => {
    const arr = [];
    await axios
      .get("http://jsonplaceholder.typicode.com/photos")
      .then((result) => {
        console.log("Result: ", result);
        for (let i = 0; i < 8; i++) {
          arr.push(result.data[i]);
        }
        setArrPic(arr);
      })
      .catch((err) => {
        console.log("err:", err);
      });
  };

  return (
    <Router>
      {/* <AxiosData/> */}
      <Menu />
      <Switch>
        <Route exact path="/home" render={() => <Home arrpic={arrpic} />} />
        <Route path="/service" render={()=><Service/>}/>
        <Route path="/allproduct/" render={() => <AllProduct />} />
        <Route path="/product/:id" render={() => <Product arrpic={arrpic} />} />
        <Route path="/auth" render={() => <Auth checkSign={checkSign}/>} />
        <Route path="/protect" render={()=><ProtectRoute/>}/>
        <ProtectRoute isSignIn={isSignIn} component={Welcome} path="/welcome" checkSign={checkSign}/>
      </Switch>
    </Router>
  );
}

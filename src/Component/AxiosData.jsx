import React,{useState,useEffect} from 'react'
import axios from 'axios';

export default function AxiosData() {

    const [users,setUsers] = useState([])
    const [id,setId] = useState("")
    const [searchUser,setSearchUser] = useState({})

    useEffect(() => {
        getDataFromApi();
    },[])

    useEffect(()=>{
        console.log("Id: ",id);
        getDataById()
    },[id])

    const getDataFromApi = async () =>{
        //TODO: axios.get = get data from api ,it have another method like put ,delete ,post
        await axios.get("http://jsonplaceholder.typicode.com/users")
        .then(result=>{
            console.log("Result: ",result);
            for(let i=0;i<5;i++){
                setUsers(result.data)
            }
        }).catch(err=>{
            console.log("err:",err)
        })
    }

    const deleteDataByID = async() =>{
        const time = [...users]
          
        for(let i=0;i<time.length;i++){
            if(time[i].id == id){
                time.splice(i,1)
            }
        }
        setUsers(time)
    }

    const getDataById = async () => {
        await axios.get(`http://jsonplaceholder.typicode.com/users/${id}`)
        .then(result=>{
            console.log("Result: ",result);
            setSearchUser(result.data)
        }).catch(err=>{
            console.log("err:",err)
        })
        // const time = [...users]
          
        // for(let i=0;i<time.length;i++){
        //     if(time[i].id == id){
        //         setSearchUser(time[i]) 
        //     }
        // }
    }

    return (
        <>
            {/*T JSON.Stringify it usre to convert data to JSON format */}
            {users.map((item,index)=>(
                <h1 key={index}>ID: {item.id} Name: {item.name}</h1>
            ))}
            <br/>
            <input type="text" value={id} onChange={(e)=>setId(e.target.value)}></input>
            {" "}
            <button onClick={getDataById}>click</button>
            {" "}
            <button onClick={deleteDataByID}>delete</button>
            <br/>
            <h1>ID: {searchUser.id}   Name:{searchUser.name}</h1>
            <br/>

        </>
    )
}


import React from "react";
import {
  Navbar,
  Nav,
  Button,
  Container
} from "react-bootstrap";
import {Link} from 'react-router-dom'

export default function Menu() {
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand as={Link} to="/home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">

              <Nav.Link as={Link} to="/home">Home</Nav.Link>
              <Nav.Link as={Link} to="/service">Video</Nav.Link>
              <Nav.Link as={Link} to="/allproduct">Account</Nav.Link>

              <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>

              <Nav.Link as={Link} to="/auth">Auth</Nav.Link>

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
